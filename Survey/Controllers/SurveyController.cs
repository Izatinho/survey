﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Survey.BL.Services;
using Survey.DAL.UnitOfWorkFactory;

namespace Survey.Controllers
{
    public class SurveyController : Controller
    {
        private readonly IQuestionService _questionService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public SurveyController(IQuestionService questionService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _questionService = questionService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<IActionResult> Index()
        {
            var questions = await _questionService.GetAllQuestionModelsAsync();
            return View(questions);
        }
    }
}
