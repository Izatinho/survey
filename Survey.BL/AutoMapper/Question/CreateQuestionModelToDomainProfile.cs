﻿using AutoMapper;
using Survey.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.BL.AutoMapper.Question
{
    public class CreateQuestionModelToDomainProfile : Profile
    {
        public CreateQuestionModelToDomainProfile()
        {
            CreateQuestionModelToQuestionMappingConfig();
        }
        private void CreateQuestionModelToQuestionMappingConfig()
        {
            CreateMap<QuestionModel, DAL.Entities.Question>()
                .ForMember(t => t.Title,
                src => src.MapFrom(x => x.Title));
        }
    }
}
