﻿using Survey.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Survey.BL.Services
{
    public interface IQuestionService
    {
        Task<List<QuestionModel>> GetAllQuestionModelsAsync();
    }
}
