﻿using AutoMapper;
using Survey.DAL.Entities;
using Survey.DAL.UnitOfWork;
using Survey.DAL.UnitOfWorkFactory;
using Survey.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Survey.BL.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IMapper _mapper;

        public QuestionService(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<QuestionModel>> GetAllQuestionModelsAsync()
        {
            using(var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var repo = uow.GetRepository<Question>();
                var questions = await repo.GetAllAsync();
                var models = _mapper.Map<List<QuestionModel>>(questions);
                return models;
            }
        }
    }
}
