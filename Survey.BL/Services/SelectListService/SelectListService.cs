﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Survey.DAL.UnitOfWorkFactory;
using System.Threading.Tasks;

namespace Survey.BL.Services.SelectListService
{
    public class SelectListService : ISelectListService
    {
        protected readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public SelectListService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<SelectList> GetSelectList<TEntity>(string fieldName) where TEntity : class, DAL.Entities.IEntity
        {
            using (var uow = _unitOfWorkFactory.MakeUnitOfWork())
            {
                System.Collections.Generic.List<TEntity> entities = await uow.GetRepository<TEntity>().GetAllAsync();
                return new SelectList(entities, nameof(DAL.Entities.IEntity.Id), fieldName);
            }
        }

    }
}
