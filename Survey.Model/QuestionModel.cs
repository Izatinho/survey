﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.Model
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
