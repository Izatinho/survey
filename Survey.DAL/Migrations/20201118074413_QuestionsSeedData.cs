﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Survey.DAL.Migrations
{
    public partial class QuestionsSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "Id", "ResponseId", "ServeyId", "Title" },
                values: new object[,]
                {
                    { 1, 0, 0, "Ваше имя?" },
                    { 2, 0, 0, "Ваше возраст?" },
                    { 3, 0, 0, "Ваш пол?" },
                    { 4, 0, 0, "Ваша дата рождения?" },
                    { 5, 0, 0, "Ваше семейное положение?" },
                    { 6, 0, 0, "Любите ли вы программирование?" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Questions",
                keyColumn: "Id",
                keyValue: 6);
        }
    }
}
