﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Survey.DAL.Migrations
{
    public partial class AddingSurveySeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Questions_Surveys_SurveyId",
                table: "Questions");

            migrationBuilder.InsertData(
                table: "Surveys",
                columns: new[] { "Id", "Title" },
                values: new object[] { 1, "Анкета" });

            migrationBuilder.CreateIndex(
                name: "IX_Surveys_Id",
                table: "Surveys",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Responses_Id",
                table: "Responses",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Questions_Id",
                table: "Questions",
                column: "Id",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Questions_Surveys_SurveyId",
                table: "Questions",
                column: "SurveyId",
                principalTable: "Surveys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Questions_Surveys_SurveyId",
                table: "Questions");

            migrationBuilder.DropIndex(
                name: "IX_Surveys_Id",
                table: "Surveys");

            migrationBuilder.DropIndex(
                name: "IX_Responses_Id",
                table: "Responses");

            migrationBuilder.DropIndex(
                name: "IX_Questions_Id",
                table: "Questions");

            migrationBuilder.DeleteData(
                table: "Surveys",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.AddForeignKey(
                name: "FK_Questions_Surveys_SurveyId",
                table: "Questions",
                column: "SurveyId",
                principalTable: "Surveys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
