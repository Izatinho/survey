﻿using Survey.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.UnitOfWorkFactory
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}
