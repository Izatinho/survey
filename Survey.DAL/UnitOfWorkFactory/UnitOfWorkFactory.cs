﻿using Survey.DAL.ContextFactory;
using Survey.DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.UnitOfWorkFactory
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IApplicationDbContextFactory _applicationDbContextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory applicationDbContextFactory)
        {
            _applicationDbContextFactory = applicationDbContextFactory;
        }

        public IUnitOfWork MakeUnitOfWork()
        {
            return new UnitOfWork.UnitOfWork(_applicationDbContextFactory.CreateContext());
        }
    }
}
