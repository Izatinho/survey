﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Survey.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.EntitiesConfiguration
{
    public class QuestionConfiguration : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder
                .HasOne(x => x.Survey)
                .WithMany(x => x.Questions)
                .HasForeignKey(x => x.SurveyId)
                .IsRequired();
            builder
                .HasIndex(x => x.Id)
                .IsUnique();

        }
    }
}
