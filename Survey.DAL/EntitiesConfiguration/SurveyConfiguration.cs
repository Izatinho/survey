﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.EntitiesConfiguration
{
    public class SurveyConfiguration : IEntityTypeConfiguration<Survey.DAL.Entities.Survey>
    {
        public void Configure(EntityTypeBuilder<Entities.Survey> builder)
        {
            builder
                .HasIndex(x => x.Id)
                .IsUnique();
            
        }
    }
}
