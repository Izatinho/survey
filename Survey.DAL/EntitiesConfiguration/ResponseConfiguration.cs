﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Survey.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.EntitiesConfiguration
{
    class ResponseConfiguration : IEntityTypeConfiguration<Response>
    {
        public void Configure(EntityTypeBuilder<Response> builder)
        {
            builder.HasIndex(x => x.Id)
                .IsUnique();
        }
    }
}
