﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Entities
{
    public interface IEntity 
    {
        int Id { get; set; }
    }
}
