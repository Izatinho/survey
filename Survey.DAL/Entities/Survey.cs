﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Entities
{
    public class Survey : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<Question> Questions { get; set; }
        
    }
}
