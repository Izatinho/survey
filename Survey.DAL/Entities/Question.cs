﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Entities
{
    public class Question : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Survey Survey { get; set; }
        public int SurveyId { get; set; }

    
        
    }
}
