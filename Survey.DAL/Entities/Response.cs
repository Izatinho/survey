﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Entities
{
    public class Response : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}
