﻿using Microsoft.EntityFrameworkCore;
using Survey.DAL.Entities;
using Survey.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.QuestionsSeed
{
    public static class SeedData
    {
        public static void Seed(this ModelBuilder builder)
        {
            
            builder.Entity<Question>().HasData(
                new Question { Id = 1, Title = "Ваше имя?" , SurveyId = 1},
                new Question { Id = 2, Title = "Ваше возраст?", SurveyId = 1 },
                new Question { Id = 3, Title = "Ваш пол?" },
                new Question { Id = 4, Title = "Ваша дата рождения?", SurveyId = 1 },
                new Question { Id = 5, Title = "Ваше семейное положение?", SurveyId = 1 },
                new Question { Id = 6, Title = "Любите ли вы программирование?", SurveyId = 1 }

                );



        }
    }
}
