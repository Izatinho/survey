﻿using Survey.DAL.Entities;
using Survey.DAL.Repositories.Repository;
using Survey.DAL.Repositories.Repository.Question;
using Survey.DAL.Repositories.Repository.Response;
using Survey.DAL.Repositories.Repository.Survey;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Survey.DAL.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        public IQuestionRepository QuestionRepository { get; }
        public ISurveyRepository SurveyRepository { get; }
        public IResponseRepository ResponseRepository { get; }

        Task<int> CompleteAsync();
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;
    }
}
