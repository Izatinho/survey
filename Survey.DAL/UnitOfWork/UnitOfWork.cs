﻿using Survey.DAL.Contex;
using Survey.DAL.Entities;
using Survey.DAL.Repositories.Repository;
using Survey.DAL.Repositories.Repository.Question;
using Survey.DAL.Repositories.Repository.Response;
using Survey.DAL.Repositories.Repository.Survey;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Survey.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;
        private readonly ConcurrentDictionary<Type, object> _repositories;

        public IQuestionRepository QuestionRepository { get; set; }
        public ISurveyRepository SurveyRepository { get; set; }
        public IResponseRepository ResponseRepository { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();

            QuestionRepository = new QuestionRepository(context);
            SurveyRepository = new SurveyRepository(context);
            ResponseRepository = new ResponseRepository(context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity
        {
            return _repositories.GetOrAdd(typeof(TEntity), new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }
    }
}
