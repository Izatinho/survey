﻿using Survey.DAL.Contex;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.ContextFactory
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateContext();
    }
}
