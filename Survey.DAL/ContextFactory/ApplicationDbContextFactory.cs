﻿using Microsoft.EntityFrameworkCore;
using Survey.DAL.Contex;

namespace Survey.DAL.ContextFactory
{
    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions _options;

        public ApplicationDbContextFactory(DbContextOptions options)
        {
            _options = options;
        }

        public ApplicationDbContext CreateContext()
        {
            return new ApplicationDbContext(_options);
        }
    }
}
