﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Enums
{
    public enum Gender
    {
        [EnumDescription("Женский")]
        Female = 1,
        [EnumDescription("Мужской")]
        Male = 2,
    }
}
