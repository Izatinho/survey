﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Enums
{
    public class EnumDescriptionAttribute : Attribute
    {
        public string Text { get; }
        public EnumDescriptionAttribute(string text)
        {
            Text = text;
        }

        
    }
}
