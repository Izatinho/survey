﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Survey.DAL.Enums
{
    public static class EnumFunctions
    {
        public static string GetDescription(this Enum enumItem)
        {
            Type type = enumItem.GetType(); //type of enum element
            var enumItemAsString = enumItem.ToString(); // "Male or Female"
            MemberInfo[] info = type.GetMember(enumItemAsString); // Metadatas about enum element

            if (info == null || !info.Any())
                return enumItemAsString;
            object[] attributes = info[0].GetCustomAttributes(typeof(EnumDescriptionAttribute), false);

            if (attributes == null || !attributes.Any())
                return enumItemAsString;

            return ((EnumDescriptionAttribute)attributes[0]).Text; 
        }
    }
}
