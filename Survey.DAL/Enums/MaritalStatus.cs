﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Enums
{
    public enum MaritalStatus
    {
        [EnumDescription("В браке")]
        Married = 1,
        [EnumDescription("Холост")]
        Unmarried = 2,
        [EnumDescription("Разведен")]
        Divorsed = 3

    }
}
