﻿using Microsoft.EntityFrameworkCore;
using Survey.DAL.Contex;
using Survey.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Survey.DAL.Repositories.Repository
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        protected ApplicationDbContext Context;
        protected DbSet<T> entities;

        public Repository(ApplicationDbContext context)
        {
            Context = context;
            entities = context.Set<T>();
        }

        public async Task<int> CreateAsync(T entity)
        {
            var entityEntry = await Context.AddAsync(entity);
            await Context.SaveChangesAsync();
            return entityEntry.Entity.Id;
        }

        public async Task<List<T>> GetAllAsync()
        {
            return await entities.ToListAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await entities.FindAsync(id);
        }

        public async Task RemoveAsync(T entity)
        {
            Context.Remove(entity);
            await Context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            Context.Update(entity);
            await Context.SaveChangesAsync();
        }
    }
}
