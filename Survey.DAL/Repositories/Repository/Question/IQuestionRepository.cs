﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Repositories.Repository.Question
{
    public interface IQuestionRepository : IRepository<Entities.Question>
    {
    }
}
