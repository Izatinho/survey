﻿using Microsoft.EntityFrameworkCore;
using Survey.DAL.Contex;

using System.Linq;


namespace Survey.DAL.Repositories.Repository.Question
{
    public class QuestionRepository : Repository<Entities.Question>, IQuestionRepository
    {
        public QuestionRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Questions;
        }

        
    }
}
