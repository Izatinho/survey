﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Repositories.Repository.Response
{
    public interface IResponseRepository : IRepository<Entities.Response>
    {
    }
}
