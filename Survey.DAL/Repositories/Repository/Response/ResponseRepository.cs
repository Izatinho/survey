﻿using Survey.DAL.Contex;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Repositories.Repository.Response
{
    public class ResponseRepository : Repository<Entities.Response>, IResponseRepository
    {
        public ResponseRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Responses;
        }

        

    }
}
