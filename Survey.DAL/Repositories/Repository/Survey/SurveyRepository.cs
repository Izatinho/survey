﻿using Microsoft.EntityFrameworkCore;
using Survey.DAL.Contex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Survey.DAL.Repositories.Repository.Survey
{
    public class SurveyRepository : Repository<Entities.Survey>, ISurveyRepository
    {
        public SurveyRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Surveys;
        }

       
    }
}
