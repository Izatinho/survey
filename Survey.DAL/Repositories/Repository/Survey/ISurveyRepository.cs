﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.DAL.Repositories.Repository.Survey
{
    public interface ISurveyRepository : IRepository<Entities.Survey>
    {
    }
}
